let navItems = document.querySelector("#navSession")

let userToken = localStorage.getItem("token")

if(!userToken){
	navItems.innerHTML = `<li class="nav-item">
	<a href="./login.html" class="nav-link text-white">Log In</a>
	</li>`
}else{
	navItems.innerHTML = `<li class="nav-item">
	<a href="./logout.html" class="nav-link text-white">Log Out</a>
	</li>`
}