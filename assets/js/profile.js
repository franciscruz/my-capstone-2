let token = localStorage.getItem("token");

let courseIds = []
let courseNames = []

fetch('https://francis-capstone-2.herokuapp.com/api/users/details', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {

	for(let i = 0; i < data.enrollments.length; i++){
		courseIds.push(data.enrollments[i].courseId)
	}

	fetch('https://francis-capstone-2.herokuapp.com/api/courses')
	.then(res => res.json())
	.then(data => {

		for(let i = 0; i < data.length; i++){

			if(courseIds.includes(data[i]._id)){
				courseNames.push(data[i].name)
			}
	}

	let coursesList = courseNames.join("\n")
  //Course names are here in coursesList
})
})